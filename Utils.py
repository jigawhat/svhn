'''
    
    Utility methods

'''

import joblib
import traceback
import itertools
import functools
from datetime import datetime
from joblib import Parallel, delayed
from copy import deepcopy
from collections import defaultdict, deque
from threading import Thread, Condition, Timer

from Constants import *


range_5 = range(5)



pr_prefix = ''

# print() and flush() output
def pr_fl(*objs):
    global pr_prefix
    print(pr_prefix, datetime.utcfromtimestamp(int(time.time())).strftime('%Y-%m-%d %H:%M:%S'), *objs)
    if sys.stdout is not None:
        sys.stdout.flush()


def save_ld(data, name, compress=3):
    create_folder(data_dir + learning_data_dir)
    joblib.dump(data, data_dir + learning_data_dir + name + ".gz", compress=compress)

def load_ld(name):
    return joblib.load(data_dir + learning_data_dir + name + ".gz")


def save_json(obj, name, pad=True):
    if pad:
        name = data_dir + name + '.json'
    with open(name, 'w') as outfile:
        json.dump(obj, outfile)

def load_json(name, pad=True, encoding=None, print_errs=False):
    if pad:
        name = data_dir + name + '.json'
    try:
        with open(name, 'r', encoding=encoding) as infile:
            return json.load(infile)
    except Exception as e:
        if print_errs:
            pr_fl("Error loading json file [" + name + "]:")
            for line in traceback.format_tb(e.__traceback__):
                pr_fl(line)
            pr_fl(type(e))
            pr_fl(e)
        return None



