

import h5py
import matplotlib.pyplot as plt
from scipy import ndimage
from skimage.transform import resize
from Utils import *




n_bbox_aug = 3
bbox_expand = 1.3
bbox_size = 64
image_size = 54
N_digs = 5
color_channels = 3
pixel_depth = 255.0



# train_set, train_labels = preprocess_samples(n_train, train_images, train_bboxes)
# save_ld((train_set, train_labels), "train_set")
# test_set, test_labels = preprocess_samples(n_test, test_images, test_bboxes, n_augmentations=1)
# save_ld((test_set, test_labels), "test_set")
train_set, train_labels = load_ld("train_set")
test_set, test_labels = load_ld("test_set")



X_train, Y_train = train_set, train_labels
X_test, Y_test = test_set, test_labels
n_train, n_test = X_train.shape[0], X_test.shape[0]
X_train.shape, [ls.shape for ls in Y_train], X_test.shape, [ls.shape for ls in Y_test]



def shuffle_data(data):
    indices = np.arange(data[0].shape[0]);
    np.random.shuffle(indices)
    return [d[indices] for d in data]
train_shuffled = shuffle_data([X_train] + Y_train)
X_train, Y_train = train_shuffled[0], train_shuffled[1:]


import tensorflow as tf
from tensorflow.contrib.layers import maxout
from tensorflow.contrib import rnn
from tensorflow.nn import conv2d, max_pool, dropout, relu
from tensorflow.layers import batch_normalization as bnorm
from IPython.display import clear_output


batch_size = 8
n_epochs = 500000
learning_rate = 0.00000015
# learning_rate_gd = 0.01
dropout_keep_prob = 0.875
train_set_test_frac = 0.2  # Get training accuracy for random 20% subset of train set
log_period_batches = 50

# Define next batch function
current_i, epochs_completed = 0, 0
def next_batch(b_size):
    global current_i, epochs_completed
    xs = X_train[current_i:current_i + b_size]
    ys = [Y[current_i:current_i + b_size] for Y in Y_train]
    current_i += b_size
    if current_i >= n_train:
        current_i = 0
        epochs_completed += 1
    return xs, ys

# Reset tensor graph
tf.reset_default_graph()
izer = tf.contrib.layers.xavier_initializer()
x = tf.placeholder(tf.float32, [None, image_size, image_size, color_channels])
ys = [tf.placeholder(tf.float32, [None, Y.shape[1]]) for Y in Y_train]
keep_p = tf.placeholder(tf.float32)
n_train_test = int(train_set_test_frac * n_train)
train_accuracy, test_accuracy, train_loss, test_loss = [], [], [], []

#####################################################
# Define model, loss, update and evaluation metric. #
#####################################################

W1 = tf.Variable(izer((5, 5, color_channels, 48))) # Conv layer 1 weights
#b1 = tf.Variable(izer((image_size, image_size, 48))) # Conv layer 1 biases
W2 = tf.Variable(izer((5, 5, 48, 64))) # Conv layer 2 weights
#b2 = tf.Variable(izer((27, 27, 64))) # Conv layer 2 biases
W3 = tf.Variable(izer((5, 5, 64, 128))) # Conv layer 2 weights
#b3 = tf.Variable(izer((27, 27, 128))) # Conv layer 2 biases
W4 = tf.Variable(izer((5, 5, 128, 160))) # Conv layer 2 weights
#b4 = tf.Variable(izer((14, 14, 160))) # Conv layer 2 biases
conv_output_neurons = 14 * 14 * 160
nl_neurons = 3072
Wl1 = tf.Variable(izer((conv_output_neurons, nl_neurons)))
bl1 = tf.Variable(izer((nl_neurons,)))
Wl2 = tf.Variable(izer((nl_neurons, nl_neurons)))
bl2 = tf.Variable(izer((nl_neurons,)))
Y_W1s = [tf.Variable(izer([nl_neurons, Y_train[i].shape[1]])) for i in range(len(Y_train))]
Y_b1s = [tf.Variable(izer([Y_train[i].shape[1]])) for i in range(len(Y_train))]

conv_1 = conv2d(x, W1, strides=[1, 1, 1, 1], padding='SAME')# + b1
maxpool_1 = dropout(maxout(bnorm(max_pool(conv_1, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')), 48), keep_p)
conv_2 = conv2d(maxpool_1, W2, strides=[1, 1, 1, 1], padding='SAME')# + b2
maxpool_2 = relu(dropout(bnorm(max_pool(conv_2, ksize=[1, 2, 2, 1], strides=[1, 1, 1, 1], padding='SAME')), keep_p))
conv_3 = conv2d(maxpool_2, W3, strides=[1, 1, 1, 1], padding='SAME')# + b3
maxpool_3 = relu(dropout(bnorm(max_pool(conv_3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')), keep_p))
conv_4 = conv2d(maxpool_3, W4, strides=[1, 1, 1, 1], padding='SAME')# + b4
maxpool_4 = relu(dropout(bnorm(max_pool(conv_4, ksize=[1, 2, 2, 1], strides=[1, 1, 1, 1], padding='SAME')), keep_p))

flattened = tf.reshape(maxpool_4, [-1, conv_output_neurons]) # Flatten
llayer_1 = dropout(tf.matmul(flattened, Wl1) + bl1, keep_p)
llayer_2 = dropout(tf.matmul(llayer_1, Wl2) + bl2, keep_p)
Y_outs = [relu(tf.matmul(llayer_2, Y_W1s[i]) + Y_b1s[i]) for i in range(len(Y_train))]

# Define loss and training step
Y_losses = [tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
             labels=ys[i], logits=Y_outs[i])) for i in range(len(Y_train))]
loss = sum(Y_losses)
train_step = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)
# train_step = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(loss)

# Define basic accuracy computation (actual house number accuracy is done later)
accuracy = sum([tf.reduce_mean(tf.cast(tf.equal(tf.argmax(
    Y_outs[i], 1), tf.argmax(ys[i], 1)), tf.float32)) for i in range(len(Y_train))]) / len(Y_train)

# Train
batch_i = 0
with tf.train.MonitoredSession() as sess:
    while epochs_completed < n_epochs:
        
        batch_i += 1
        batch_xs, batch_ys = next_batch(batch_size)
        ys_feed = {ys[i]: batch_ys[i] for i in range(len(Y_train))}
        sess.run(train_step, feed_dict={**ys_feed,**{x: batch_xs, keep_p: dropout_keep_prob}})

        if batch_i % log_period_batches == 0:

            # Get random sample of 20% of the training dataset
            indices = np.random.choice(n_train, n_train_test, replace=False)
            ys_feed = {ys[i]: Y_train[i][indices] for i in range(len(Y_train))}
            train_l, train_a = sess.run([loss, accuracy], feed_dict={**ys_feed,**{
                                                          x: X_train[indices], keep_p: 1.0}})

            ys_feed = {ys[i]: Y_test[i] for i in range(len(Y_train))}
            test_l, test_a = sess.run([loss, accuracy], feed_dict={**ys_feed,**{
                                                        x: X_test, keep_p: 1.0}})
            
            train_loss += [ train_l ]
            train_accuracy += [ train_a ]
            test_loss += [ test_l ]
            test_accuracy += [ test_a ]
            
            clear_output()
            print("Epoch", epochs_completed, ':', train_a, test_a, train_l, test_l, " Best test acc:", max(test_accuracy))
            fig = plt.figure()
            fig.set_size_inches(16, 5)
            g = fig.add_subplot(1,2,1)
            g.grid()
            g.plot(train_accuracy, label='train acc')
            g.plot(test_accuracy, label='test acc')
            g.legend(loc='lower right')
    
            g = fig.add_subplot(1,2,2)
            g.grid()
            g.plot(train_loss, label='train loss')
            g.plot(test_loss, label='test loss')
            g.legend(loc='upper right')
    
            plt.show()